# Bachelor thesis - A Serious Game

This serious game was created during my bachelor thesis at TU Braunschweig. 
It's is about the digitalisation of a board game a the messurement of a learning effect.
A result evaluation was done by a pre- and post-test used by the study participants.

Processing period: Nov. 2019 - Jan. 2020

Available at: [link](https://bachelorarbeit.vercel.app/start)

Thesis structure:

I. Abstract

1. Introduction
2. Basic terms
3. Literatur review
4. Development
5. Study
6. Conclusion
7. Literature

## Features

- Round based serious game up to four players
- 8 playable characters with background stories
- 2 different stories (each has 4 characters)
- Answer question correct to gain a game bonus
- The player who reaches first the goal has won
- The game is finised until only one player hasn't reached the goal

## Preview


### Stories
<img src="res/screenshots/Stories.png"  width="968" height="500">

### Characters
<img src="res/screenshots/Characters.png"  width="707" height="500">

### Game
<img src="res/screenshots/Game.png"  width="955" height="500">
